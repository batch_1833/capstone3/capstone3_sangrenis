import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext";
import {Navigate, useNavigate, Link} from "react-router-dom";

import Swal from "sweetalert2";
import {Button, Form, Modal} from "react-bootstrap";



export default function AddCourse(){


	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	//State hooks to store the values of the input fields
	const [name, setCourseName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [slots, setSlots] = useState("");

	//Check if the values are successfully binded/passed.
	console.log(name);
	console.log(description);
	console.log(price);
	console.log(slots);


	
	function addCourse(e){
		//prevents the page redirection via form submit
		e.preventDefault();

				fetch(`${process.env.REACT_APP_API_URL}/courses/addCourse`, {
					method: "POST",
					headers:{
						"Content-Type": "application/json",
						Authorization: `Bearer ${localStorage.getItem("token")}`
					},
					body: JSON.stringify({
						name: name,
						description: description,
						price: price,
						slots: slots,
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data){
						//Clear input fields
						setCourseName("");
						setDescription("");
						setPrice("");
						setSlots("");

						Swal.fire({
							title: "Completed",
							icon: "success",
							text: "Course successfully added!"
						})

						
						navigate("/admin");
					}
					else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
		

	//State to determine whether submit button is enabled or not.
	const [isActive, setIsActive] = useState(false);

	// To enable the submit button:

	useEffect(()=>{
		if((name !== "" && description !== "" && price !== "" && slots !== "")){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[name, description, price, slots])



	return(
		(user.isAdmin)
		?
		<>
		<div className="mt-5 mb-3 text-center">
		<h1>Admin Dashboard</h1>
		{/*A button to add a new course*/}
		<Button as={Link} to="/addCourse" variant="primary" size="lg" className="mx-2" >Add Course</Button>
		<Button as={Link} to="/admin" variant="danger" size="lg" className="mx-2" >Courses</Button>
		<Button as={Link} to="/allUsers" variant="secondary" size="lg" className="mx-2" >Users List</Button>
		<Button variant="success" size="lg" className="mx-2">Enrollments</Button>
    	</div>
			<Form  onSubmit = {(e) => addCourse(e)}>

				<Form.Group className="mb-3" controlId="name">
				  <Form.Label>Course Name</Form.Label>
				  <Form.Control type="text" placeholder="Course Name" value={name} onChange={e => setCourseName(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="description">
				  <Form.Label>Course description</Form.Label>
				  <Form.Control type="text" placeholder="description" value={description} onChange={e => setDescription(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="price">
				  <Form.Label>Price</Form.Label>
				  <Form.Control type="number" placeholder="price" value={price} onChange={e => setPrice(e.target.value)}/>
				</Form.Group>

			      <Form.Group className="mb-3" controlId="slots">
			        <Form.Label>Slots</Form.Label>
			        <Form.Control type="slots" placeholder="Slots" value={slots} onChange={e => setSlots(e.target.value)}/>
			      </Form.Group>
		      
		      {
		      	isActive
		      	?
		      		<Button variant="primary" type="submit" id="submitBtn">
		      		  Submit
		      		</Button>
		      	:
		      		<Button variant="primary" type="submit" id="submitBtn" disabled>
		      		  Submit
		      		</Button>
		      }
		    </Form>
    </>

		:
		<Navigate to="/courses" />
	)
}