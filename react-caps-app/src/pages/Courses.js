import {useEffect, useState, useContext} from "react";
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";

import CourseCard from "../components/CourseCard";

export default function Courses(){

	//State tha will be used to store the courses retrieved form the database
	 const [courses, setCourses] = useState([]);

	 // To be used for validating the "role" of the user to login.
	 const {user} = useContext(UserContext);

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/courses`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data.map(course =>{
				return(
					<CourseCard key={course._id} courseProp={course} />
				)
			}))
		})
	},[])

	return(
		(user.isAdmin)
		?
			<Navigate to ="/admin" />
		:
		<>
			<h1 className="my-5 text-center">Courses</h1>
			{courses}
		</>
	)
}
