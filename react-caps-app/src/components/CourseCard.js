import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Card, Button, Col, Row} from "react-bootstrap";

//Deconstruct the "courseProp" form the "props" object to shorten syntax.
export default function CourseCard({courseProp}){

const {_id, name, description, price, slots} = courseProp;
return (

	<Row className="mt-3 mb-3">
	  <Col>
		<Card className="cardHighlight p-3">
		    <Card.Body>
		        <Card.Title>
		            {name}
		        </Card.Title>
		        <Card.Subtitle>Description: </Card.Subtitle>
		        <Card.Text>
		            {description}
		        </Card.Text>
		        <Card.Subtitle>Price: </Card.Subtitle>
		        <Card.Text>
		            {price}
		        </Card.Text>
		        <Card.Text>
		            Slots: {slots}
		        </Card.Text>
		    	{/*We will be able to select a specific course through its url*/}
		        <Button as={Link} to={`/courses/${_id}`} variant="primary">Details</Button>

		        {/*<Button variant="primary mx-1" onClick={enroll}>Enroll</Button>
		        <Button variant="danger mx-1" onClick={unEnroll}>Unenroll</Button>*/}
		    </Card.Body>
		</Card>

	</Col>
	</Row>
	)
}