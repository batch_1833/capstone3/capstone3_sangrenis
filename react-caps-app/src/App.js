import {useState, useEffect} from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import {Container} from "react-bootstrap";
import { UserProvider } from "./UserContext";
import AppNavbar from "./components/AppNavbar";

import UsersList from "./pages/UsersList"
import AdminDashboard from "./pages/AdminDashboard"
import Courses from "./pages/Courses";
import UpdateCourse from "./pages/UpdateCourse";
import CourseView from "./pages/CourseView";
import Error from "./pages/Error";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";
import AddCourse from "./pages/AddCourse";


import './App.css';


function App() {

  const [user, setUser] = useState({
            //null
    // email: localStorage.getItem("email")
    id: null,
    isAdmin: null
  })

         //Function for clearing localStorage on logout
  const unsetUser = () =>{
    localStorage.clear();
  }

 return (


    <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar />
          <Container fluid>
              <Routes>
               <Route exact path ="/" element={<Home />} />
               <Route exact path ="/admin" element ={< AdminDashboard />}/>
               <Route exact path ="/allUsers" element ={< UsersList />}/>
               <Route exact path ="/addCourse" element ={< AddCourse />}/>
               <Route exact path ="/updateCourse/:courseId'" element={<UpdateCourse />} />
                <Route exact path ="/courses" element={<Courses />} />
                  <Route exact path ="/courses/:courseId" element={<CourseView />} />
                  <Route exact path ="/register" element={<Register />} />
                  <Route exact path ="/login" element={<Login />} />
                  <Route exact path ="/logout" element={<Logout />} />
                  <Route exact path ="*" element={<Error />} />
              </Routes>
          </Container>
        </Router>
    </UserProvider>
  );
}


export default App;
