const Course = require("../models/Course");


// to add course
module.exports.addCourse = (reqBody) =>{

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	})

	return newCourse.save().then((course, error) =>{
		if(error){
			return false
		}
		else{
			return true
		}
	})
}



// RETRIEVE ALL COURSE
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => result);
}


module.exports.getAllActive = () =>{
	return Course.find({isActive:true}).then(result => result);
}


module.exports.getCourse = (courseId) =>{
	return Course.findById(courseId).then(result => result);
}



module.exports.updateCourse = (courseId, reqBody) =>{
	// Specify the fields/properties to be updated
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	}

	// Syntax: findByIdAndUpdate(documentId, updatesToBeApplied)
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((courseUpdate, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}



//Archiving a course
module.exports.archiveCourse = (courseId, reqBody) =>{
	let updateActiveField = {
		isActive : reqBody.isActive
	}

	return Course.findByIdAndUpdate(courseId, updateActiveField).then((isActive, error) =>{
		// Course is not archived
		if(error){
			return false;
		}
		// Course archived successfully
		else{
			return true
		}
	})
}


